#!/usr/bin/env python
"""Example of RandomEnv environment that generates random observations and rewards."""
# pylint: disable=invalid-name

from gym.spaces import Discrete
from gymbag import RandomEnv


env = RandomEnv(observation_space=Discrete(10), action_space=Discrete(2), episode_steps=10)

for episode in range(2):
    env.reset()
    done = False
    while not done:
        obs, reward, done, info = env.step(env.action_space.sample())
        print(obs, reward, done, info)

env.close()
