#!/usr/bin/env python
"""Gymbag environment playback example.  Uses cartpole data from record.py."""
# pylint: disable=invalid-name

from gymbag import HDF5Reader, PlaybackEnv

env = PlaybackEnv(HDF5Reader('cartpole.h5'))

while not env.played_out:
    env.reset()
    done = False
    while not done:
        obs, reward, done, info = env.step(env.action_space.sample())
        print(obs, reward, done, info)

env.close()
