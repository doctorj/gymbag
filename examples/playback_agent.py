#!/usr/bin/env python
"""Gymbag agent playback example.  Uses cartpole data from record.py.
This is mostly useful with deterministic environments."""
# pylint: disable=invalid-name

import gym
from gymbag import HDF5Reader, PlaybackAgent

env = gym.make('CartPole-v0')
agent = PlaybackAgent(HDF5Reader('cartpole.h5'))

while agent.next_episode():
    env.reset()
    done = False
    while not done and not agent.done:
        obs, reward, done, info = env.step(agent.act())
        print(obs, reward, done, info)

env.close()
