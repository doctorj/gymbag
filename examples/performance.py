#!/usr/bin/env python
"""Test Gymbag performance."""

import sys
import time
import shutil
import logging
from collections import namedtuple
import itertools
from typing import Any, Tuple
import os
try:
    from os import scandir
except ImportError:
    from scandir import scandir

import gym
import gym_recording
import gym_recording.wrappers
import gym_recording.playback

from gymbag import RandomAgent, record_hdf5, HDF5Reader


Result = namedtuple('Result', ('recorder', 'env', 'episodes', 'steps', 'read_secs', 'record_secs', 'disk_bytes'))


def dir_bytes(path='.'):
    """:Return: total bytes used by all files in a directory and its subdirectories recursively."""
    total = 0
    for entry in scandir(path):
        if entry.is_file(follow_symlinks=False):
            total += entry.stat().st_size
        elif entry.is_dir(follow_symlinks=False):
            total += dir_bytes(entry.path)
    return total


def measure_bare(env: gym.Env, agent: Any, episodes: int) -> Tuple[int, float, float, int]:
    """:Return: the total steps, "read CPU seconds" (0), record CPU seconds, and "disk bytes" (0) for running a bare environment with no recording wrapper."""
    steps = 0
    start = time.process_time()
    for _ in range(episodes):
        obs = env.reset()
        done = False
        while not done:
            obs, _, done, _ = env.step(agent.act(obs))
            steps += 1
    env.close()
    record_cpu_secs = time.process_time() - start
    return steps, 0.0, record_cpu_secs, 0


def measure_monitor(env: gym.Env, agent: Any, episodes: int, filename: str, delete: bool = True) -> Tuple[int, float, float, int]:
    """:Return: the total steps, "read CPU seconds" (0), record CPU seconds, and "disk bytes" (0) for running an environment wrapped with a :class:`gym.wrappers.Monitor`.

    Does not actually record all environment data, only summary stats.  Does not record video.
    """
    record_steps = 0
    env = gym.wrappers.Monitor(env, filename, video_callable=False, force=True)
    start = time.process_time()
    for _ in range(episodes):
        obs = env.reset()
        done = False
        record_steps += 1
        while not done:
            obs, _, done, _ = env.step(agent.act(obs))
            record_steps += 1
    env.close()
    record_cpu_secs = time.process_time() - start

    bytes_used = dir_bytes(filename)

    start = time.process_time()
    results = gym.monitoring.load_results(filename)
    read_cpu_secs = time.process_time() - start

    read_steps = sum(results['episode_lengths']) + len(results['episode_lengths'])      # Monitor doesn't count reset as a step
    assert read_steps == record_steps, 'recorded {} read {}'.format(record_steps, read_steps)

    if delete:
        shutil.rmtree(filename)

    return record_steps, read_cpu_secs, record_cpu_secs, bytes_used


def measure_recording(env: gym.Env, agent: Any, episodes: int, filename: str, delete: bool = True) -> Tuple[int, float, float, int]:
    """:Return: the total steps, "read CPU seconds" (0), record CPU seconds, and "disk bytes" (0) for running an environment wrapped with a :class:`gym_recording.wrappers.TraceRecordingWrapper`.

    Does not actually use `filename`.
    """
    record_steps = 0
    read_steps = 0
    env = gym_recording.wrappers.TraceRecordingWrapper(env)     # Does not use filename due to bug
    filename = env.directory

    start = time.process_time()
    for _ in range(episodes):
        obs = env.reset()
        done = False
        record_steps += 1
        while not done:
            obs, _, done, _ = env.step(agent.act(obs))
            record_steps += 1
    env.close()
    record_cpu_secs = time.process_time() - start

    bytes_used = dir_bytes(filename)

    def episode_callback(observations, actions, rewards):
        """Callback"""
        # pylint: disable=unused-argument
        nonlocal read_steps
        if hasattr(observations, 'shape'):
            read_steps += observations.shape[0]
        else:
            read_steps += len(observations)     # Buuuuugz...

    start = time.process_time()
    gym_recording.playback.scan_recorded_traces(filename, episode_callback)
    read_cpu_secs = time.process_time() - start

    assert read_steps == record_steps, 'recorded {} read {}'.format(record_steps, read_steps)

    if delete:
        shutil.rmtree(filename)

    return record_steps, read_cpu_secs, record_cpu_secs, bytes_used


def measure_hdf5(env: gym.Env, agent: Any, episodes: int, filename: str, delete: bool = True) -> Tuple[int, float, float, int]:
    """:Return: Total steps, read CPU seconds, record CPU seconds, and disk bytes for running `env` with an HDF5 recorder and `agent` for `episodes`.

    :param delete: If True, delete files after measuring.
    """
    try:
        os.remove(filename)
    except IOError:
        pass
    # Record
    env = record_hdf5(env, filename)
    record_steps = 0
    start = time.process_time()
    for _ in range(episodes):
        obs = env.reset()
        done = False
        record_steps += 1       # Reset counts as a step, since we store it
        while not done:
            obs, _, done, _ = env.step(agent.act(obs))
            record_steps += 1
    env.close()
    record_cpu_secs = time.process_time() - start
    # Size
    bytes_used = os.path.getsize(filename)

    # Read
    start = time.process_time()
    read_steps = sum(sum(1 for _ in episode) for episode in HDF5Reader(filename))
    read_cpu_secs = time.process_time() - start
    assert read_steps == record_steps

    # Cleanup
    if delete:
        os.remove(filename)

    return record_steps, read_cpu_secs, record_cpu_secs, bytes_used


def get_env_name(env: gym.Env) -> str:
    """:Return: the `env` name, or class name if no spec."""
    return env.spec.id if hasattr(env, 'spec') else env.__class__.__name__


def main():
    """Run a set of environments with all recorders and measure performance."""
    logging.getLogger('gym').setLevel(logging.WARNING)
    envs = (        # environment, episodes
        (gym.make('CartPole-v0'), 1000),
        (gym.make('Breakout-v0'), 10),
    )
    seed = 123
    delete = True

    def print_result(result):
        """Pretty-print `result` tuple as TSV."""
        print(result.recorder, result.env, result.episodes, result.steps, round(result.read_secs, 2), round(result.record_secs, 2), result.disk_bytes // 1024, sep='\t')

    results = []
    print(*(Result._fields[:-1] + ('disk_kb',)), sep='\t')
    for env, episodes in envs:
        name = get_env_name(env)
        env.seed(seed)
        results.append(Result._make(('none', name, episodes) + measure_bare(env, RandomAgent(env.action_space, seed), episodes)))
        print_result(results[-1])       # People are impatient.  :)
        env.seed(seed)
        results.append(Result._make(('gymbag.hdf5', name, episodes) + measure_hdf5(env, RandomAgent(env.action_space, seed), episodes, name + '.h5', delete)))
        print_result(results[-1])
        env.seed(seed)
        results.append(Result._make(('gym.Monitor', name, episodes) + measure_monitor(env, RandomAgent(env.action_space, seed), episodes, name + '.monitor', delete)))
        print_result(results[-1])
        env.seed(seed)
        results.append(Result._make(('gym_recording', name, episodes) + measure_recording(env, RandomAgent(env.action_space, seed), episodes, '', delete)))
        print_result(results[-1])

    # Output performance as a factor of the best.  Groupby env, divide by min for each of read, record, bytes.
    # Where's Pandas when you need it...
    print(file=sys.stderr)
    print('recorder', 'env', 'read_secs_x', 'record_secs_x', 'disk_bytes_x', sep='\t', file=sys.stderr)
    for env, env_results in itertools.groupby(results, lambda r: r.env):
        env_results = list(env_results)
        min_record_secs = min(result.record_secs for result in env_results)
        min_read_secs = min(filter(None, (result.read_secs for result in env_results)))
        min_disk_bytes = min(filter(None, (result.disk_bytes for result in env_results)))
        for result in env_results:
            print(result.recorder, result.env, '{:.1f}X'.format(result.read_secs / min_read_secs), '{:.1f}X'.format(result.record_secs / min_record_secs), '{:.1f}X'.format(result.disk_bytes/ min_disk_bytes), sep='\t', file=sys.stderr)


if __name__ == '__main__':
    main()
