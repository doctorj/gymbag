#!/usr/bin/env python
"""Tests for Gymbag core."""

# Copyright (C) 2017  Doctor J
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import itertools
import unittest
from typing import Iterable, Any

import gym
import gym.spaces
import numpy as np

from gymbag import ListRecorder, PlaybackEnv
from gymbag.core import TObs, TData, TAct, generate_monotonic, record_from_iter, RandomEnv, generate_random, drive_env, PlaybackAgent, get_actions

ACTION_SPACES = (gym.spaces.Discrete(2), gym.spaces.Box(-1, 1, shape=(1,)), gym.spaces.Box(-10, 10, shape=(2, 3)))
OBSERVATION_SPACES = (gym.spaces.Discrete(2), gym.spaces.Box(-1, 1, shape=(1,)), gym.spaces.Box(-10, 10, shape=(2, 3)), gym.spaces.Box(0, 255, shape=(210, 160, 3)))


def steps_equal(step1: TData[TObs, TAct], step2: TData[TObs, TAct], places: int = 6, ignore_time: bool = False, ignore_info: bool = False) -> bool:
    """Asset that two steps are approximately equal.

    :param places: Compare floats to this many decimal places.
    """
    # pylint: disable=too-many-locals
    if step1 is None or step2 is None:
        return False
    (utime1, obs1, act1, reward1, done1, info1), (utime2, obs2, act2, reward2, done2, info2) = step1, step2
    obs1, obs2 = np.asarray(obs1), np.asarray(obs2)
    act1, act2 = np.asarray(act2), np.asarray(act2)
    return all((
        ignore_time or np.allclose(utime1, utime2, atol=10 ** -places),
        np.allclose(obs1, obs2, equal_nan=True, atol=10 ** -places),
        np.allclose(act1, act2, equal_nan=True, atol=10 ** -places),
        np.allclose(reward1, reward2, equal_nan=True, atol=10 ** -places),
        done1 == done2,
        ignore_info or (not info1 and not info2) or (info1 == info2)        # So None and {} compare equal
    ))


def assert_episodes_equal(data1: Iterable[Iterable[TData[TObs, TAct]]], data2: Iterable[Iterable[TData[TObs, TAct]]], places: int = 6, ignore_time: bool = False, ignore_info: bool = False) -> None:
    """Assert that two iterables of episode data are equal."""
    for episode, (ep1, ep2) in enumerate(itertools.zip_longest(data1, data2)):
        for step, (step1, step2) in enumerate(itertools.zip_longest(ep1, ep2)):
            assert steps_equal(step1, step2, places=places, ignore_time=ignore_time, ignore_info=ignore_info), 'Episode {} step {} (0-based) differ:\n{}\n{}'.format(episode, step, step1, step2)


class GymbagTest(unittest.TestCase):
    # pylint: disable=invalid-name,missing-docstring
    def test_steps_equal(self) -> None:
        st1 = (0, 1, 2, 3, False, None)
        trues = ((st1, st1), )
        falses = ((st1, None), (None, st1),)
        for a, b in trues:
            self.assertTrue(steps_equal(a, b), '{} {}'.format(a, b))
        for a, b in falses:
            self.assertFalse(steps_equal(a, b), '{} {}'.format(a, b))

    def test_record_from_iter(self) -> None:
        # pylint: disable=no-self-use
        data = list(map(list, generate_monotonic()))
        recorder = ListRecorder()
        record_from_iter(recorder, data)
        assert_episodes_equal(recorder.data, data)

    def test_RandomEnv(self) -> None:
        env = RandomEnv[float, int](gym.spaces.Box(0, 1, shape=(1,)), gym.spaces.Discrete(2), episode_steps=10)
        done = False
        env.reset()
        while not done:
            obs, reward, done, _ = env.step(env.action_space.sample())
            self.assertTrue(env.observation_space.contains(obs))
            self.assertTrue(0.0 <= reward <= 1.0)

    def test_ListRecorder(self) -> None:
        for obs_space, act_space in itertools.product(OBSERVATION_SPACES, ACTION_SPACES):
            recorder = ListRecorder[Any, Any]()
            episodes = list(map(list, generate_random(obs_space.sample, act_space.sample)))
            for episode in episodes:
                episode = iter(episode)
                recorder.on_reset(*next(episode))
                for data in episode:
                    recorder.on_step(*data)
            recorder.on_close()

            self.assertListEqual(recorder.data, episodes)

    def test_PlaybackEnv(self) -> None:
        # pylint: disable=no-self-use
        episodes = 10
        data = list(map(list, generate_monotonic(episodes)))
        env = PlaybackEnv(data, gym.spaces.Box(0, episodes, shape=(2,)), gym.spaces.Discrete(episodes))
        actual = list(map(list, drive_env(env, episodes=episodes)))
        assert_episodes_equal(data, actual, ignore_time=True)

        # Stop early with number of episodes
        env = PlaybackEnv(data[:episodes - 3], gym.spaces.Box(0, episodes, shape=(2,)), gym.spaces.Discrete(episodes))
        actual = list(map(list, drive_env(env, episodes=episodes - 3)))
        assert_episodes_equal(data[:episodes - 3], actual, ignore_time=True)

        # Stop episodes early with steps_per_episode
        steps_per_episode = 7
        env = PlaybackEnv(data, gym.spaces.Box(0, episodes, shape=(2,)), gym.spaces.Discrete(episodes))
        actual = list(map(list, drive_env(env, episodes=episodes, steps_per_episode=steps_per_episode)))
        data_short = [steps[:steps_per_episode + 1] for steps in data]
        assert_episodes_equal(data_short, actual, ignore_time=True)

    def test_PlaybackAgent(self) -> None:
        # pylint: disable=no-self-use
        episodes = 10
        max_steps = 7
        data = list(map(list, generate_monotonic(episodes)))
        agent = PlaybackAgent(data)
        # Slice out actions, starting from step 1 since step 0 action is a bogon
        # And for the last few, stop early to test reset before done
        expected = [list(episode)[:max_steps] for episode in get_actions(data)]
        actual = []
        while agent.next_episode():
            actual.append([])
            step = 0
            while step < max_steps and not agent.done:
                actual[-1].append(agent.act())
                step += 1

        for exp_ep, act_ep in itertools.zip_longest(expected, actual):
            for exp_act, act_act in itertools.zip_longest(exp_ep, act_ep):
                np.testing.assert_allclose(act_act, exp_act)


if __name__ == "__main__":
    unittest.main()
