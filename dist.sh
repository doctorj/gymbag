#!/bin/bash

# Gymbag 'build'/upload/test script.

set -eo pipefail
if [[ $# -ne 2 ]]; then
    echo 'Usage: $0 upload_url download_url' >&2
    exit 1
fi

project=gymbag      # NO SPACES
user=doctorj
upload_url=$1
download_url=$2
tmp=$(mktemp -td "$project"XXXXXX)
dir=$(pwd)
export all_proxy= ALL_PROXY=

# Run as root to get a clean tarball
sudo bash -c "
    . python/bin/activate
    rm -rf dist/ $project.egg-info/
    python setup.py sdist
    chown -R $USER:$USER dist/ $project.egg-info/
"

# Upload
twine upload -u "$user" --repository-url "$upload_url" "dist/$project"-*
sleep 5        # PyPI is eventually consistent...


# Download
pushd "$tmp"
python -m venv "$project"
. "$project"/bin/activate
pip install --upgrade pip
pip install --index-url "$download_url"/simple --extra-index-url https://pypi.org/simple "$project"

# Test
# For reasons inexplicable, it is impossible for Python to install files in your source dir (but not package dir) to the package dir.
python -m unittest discover -v -s "$dir"
python "$dir/examples/record.py"
python "$dir/examples/playback.py"

deactivate
popd
rm -rf dist/ "$project.egg-info/"
rm -rf "$tmp"
